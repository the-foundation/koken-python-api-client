# v1.0 of a command line uploader for the koken gallery software.
# The only non-standard requirement is pyton-requests.


#counterpart https://github.com/kassi/koken/blob/master/app/application/controllers/contents.php
import json
import requests
import os
import sys
import getpass 

try:
    from urllib import quote  # Python 2.X
except ImportError:
    from urllib.parse import quote  # Python 3+


#### API URL ###
try:
    koksen = raw_input("Koken url WITHOUT PROTOCOL (e.g mysite.com/koken OR mysite.com):")  # Python 2.X
except NameError:
    koksen = input("Koken url WITHOUT PROTOCOL (e.g mysite.com/koken OR mysite.com):")  # Python 3+

api_url= 'https://' + koksen + '/api.php?'  
##api_url = "https://yourdomain.local/api.php?"
print ('API URL:', api_url );
common_headers = {'X-Koken-Auth':'cookie'}

#### username ###
try:
    user = raw_input("Username(emai):")  # Python 2.X
except NameError:
    user = input("Username:(email)")  # Python 3+

email = quote(user) 
#############or ('your@mail.local')


try: 
        password = getpass.getpass() 
except Exception as error: #
	print('ERROR', error) 
else: 
	print('Password entered...') 

# Establish an authenticated session
auth = requests.Session()
auth.post(api_url + '/sessions',
#    data={'email': email, 'token': token},
    data={'email': email, 'password': password},
#    data={'X-Koken-Token': token,'email': email},
    headers=common_headers)

def create_album(title):
    res = auth.post(api_url + '/albums',
        data={'title': title, 'album_type': '0', 'listed': '0'},
        headers=common_headers)    
    print('ALBUM RESPONSE:',res.status_code)
##returns monster json
#    print(res.text);
    msg = json.loads(res.text)
    id = msg['id'];
    log('Created album: %s, id: %s' % (title, id))
    return(id)

def upload_localfile(file):
    print('↑↑:',file);
    files = {'file': open(file,'rb')}
#    files = dict(file=open(file,'rb'));
    files = {'file':(file, open(file, 'rb'), "multipart/form-data")};

    res = auth.post(api_url + '/content', files=files,
        data={'name': file ,'localfile': file,'visibility': 'unlisted'},
#        data={'localfile': file, 'visibility': 'public'},
        headers=common_headers)
    print('UPLOAD RESPONSE:',res.status_code)
##returns monster json
    if res.status_code!= 200 :
          print(res.text);
          #from pprint import pprint
          print(res.headers)
    msg = json.loads(res.text)
    id = msg['id'];
#    id = res.text.json['id']
    log('Uploaded localfile %s, id: %s' % (file, id))
    return(id)

def add_content_to_album(content_id, album_id):
    res = auth.post(api_url + '/albums/%s/content/%s' % (album_id, content_id),
        data={'localfile': file, 'visibility': 'public'},
        headers=common_headers)
    log('Added %s to album %s' % (content_id, album_id))
    return

def log(msg):
    print(msg)


# Create an album named the same as your current working directory,
# and upload each file within to the new album.
cwd = os.getcwd()
dname = cwd.split('/')[-1]
album_id = create_album(dname)

for file in os.listdir(cwd):
     content_id = upload_remotefile(file)
     add_content_to_album(content_id, album_id)
