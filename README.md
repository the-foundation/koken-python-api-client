# Koken Python API client

* python3 uploader for koken  (should work with python2  , but untested since py2 deprecation came earlier )
* username/password auth ( token auth planned )
* works only up to UPLOAD_MAX_FILESIZE ( multipart refused to work , you can use the url script and let the server get your image from another url ;)  )
* except the import-list script , the current folder name becomes the album name
* creates a new album each run



* imports:

```
import json
import requests
import os
import sys
import getpass 

```


##### inspired by: 
- gist.github.com/hcooper/71dc46d292ecfdc37e4e
- gist.github.com/api984/161f5e86d532e62901b94e9a5bc71489
- gist.github.com/gorodok11/00ccb5e1039704eea994
- gist.github.com/matgou/1e9c332bd0bf422b2751

---

## Scripting Examples

you still have to enter your password

in the examples, the album folders are __below__ the directory of the scripts , so change `../` accordingly

###scripting url list fetch ( takes album name from console)

`(sleep 0.2;echo $uri;echo $mail ;sleep 0.2 ;echo AlbumNAMEhere)|python3 ../koken-upload-url-list.py`

##scriping url fetch

`(sleep 0.2;echo $uri;echo $mail ;sleep 0.2 ;echo https://upload.wikimedia.org/wikipedia/commons/e/e6/Clocktower_Panorama_20080622_20mb.jpg)|python3 ../koken-upload-url-simple.py`


<h3>A project of the foundation</h3>
<a href="https://the-foundation.gitlab.io/"><div><img src="https://hcxi2.2ix.ch/gitlab/the-foundation/docker-typo3/README.md/logo.jpg" width="480" height="270"/></div></a>

